from array import *
import random
import os
import readchar

import colorama
from colorama import init
from colorama import Fore, Back, Style
colorama.init()


counter = 0
overflow = 0
flag = True

def cardcreate(x):
	global deck, carts, botcarts
	suit = random.randint(0, 3)
	num = random.randint(0, 8)
	while deck[suit][num] == '':
		num = random.randint(0, 8)
	if x == 2: 
		if decksuit[suit] == '♥' or decksuit[suit] == '♦':
			botcarts.append(Fore.RED + Back.WHITE + decksuit[suit] + Style.RESET_ALL + deck[suit][num])
		else:
			botcarts.append(Fore.BLACK + Back.WHITE + decksuit[suit] + Style.RESET_ALL + deck[suit][num])
	if x == 1:
		if decksuit[suit] == '♥' or decksuit[suit] == '♦':
			carts.append(Fore.RED + Back.WHITE + decksuit[suit] + Style.RESET_ALL + deck[suit][num])
		else:
			carts.append(Fore.BLACK + Back.WHITE + decksuit[suit] + Style.RESET_ALL + deck[suit][num])

	deck[suit][num] = ''
	if num != 3:
		return num + 2
	elif num == 3:
		return 11


def rules():
	print('*', 61 * '-', '*', end='\n'
		'|                             Правила:                          |\n'
		'| ~ Цель игры - набрать 21 очко (или число очков,               |\n'
		'|    наиболее приближенное к 21) , но не более 21               |\n'
        '|       Очки начисляются следующим образом:                     |\n'
        '|  ~ Валет — 2 очка                                             |\n'
        '|  ~ Дама — 3 очка                                              |\n'
        '|  ~ Король — 4 очка                                            |\n'
        '|  ~ Туз — 11 очков                                             |\n'
        '|  ~ Остальные карты при подсчете очков оцениваются по номиналу |\n')
	print('*', 61 * '-', '*', end='\n')


os.system("cls")
print('*', 20 * '-', '*', end='\n'
          '|   Привет, сыграем?   |\n')
print('*', 20 * '-', '*', end='\n')
print('Y/N')
choice = input()
os.system("cls")
while flag:
	deck = [['В', "Д", 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10']]
	decksuit = ['♠', '♥', '♣', '♦']

	done = False
	summ = 0
	carts = []
	botcarts = []
	overflow = 0
	if counter == 0:
		pass
	else:
		print('*', 12 * '-', '*', end='\n'
        	  '|   Ещё раз?   |\n')
		print('*', 12 * '-', '*', end='\n')
		print('Y/N')
		choice = input()
		os.system("cls")
	if choice == 'y' or choice == 'Y' or choice == 'н' or choice == 'Н':
		summ += cardcreate(1)
		pol = 1

		os.system("cls")
		rules()
		print("Карты:", end=" ")
		for i in range(len(carts)):
			print(carts[i], end=" ")
		print("")
		if pol == 1:
			print(">Взять ещё \n Хватит")
			pol = 2
		elif pol == 2:
			print(" Взять ещё \n>Хватит")
			pol = 1 


		while not done:
			key = readchar.readkey()
			if key == "\x1b[A":
				os.system("cls")
				rules()
				print("Карты:", end=" ")
				for i in range(len(carts)):
					print(carts[i], end=" ")
				print("")
				if pol == 1:
					print(">Взять ещё \n Хватит")
					pol = 2
				elif pol == 2:
					print(" Взять ещё \n>Хватит")
					pol = 1 
			elif key == "\x1b[B":
				os.system("cls")
				rules()
				print("Карты:", end=" ")
				for i in range(len(carts)):
					print(carts[i], end=" ")
				print("")
				if pol == 1:
					print(">Взять ещё \n Хватит")
					pol = 2
				elif pol == 2:
					print(" Взять ещё \n>Хватит")
					pol = 1
			elif key == "\r":
				if pol == 2:
					overflow += 1
					if overflow > 9:
						break
					summ += cardcreate(1)
					os.system("cls")
					rules()
					print("Карты:", end=" ")
					for i in range(len(carts)):
						print(carts[i], end=" ")
					print("")

					if pol == 2:
						print(">Взять ещё \n Хватит")
					elif pol == 1:
						print(" Взять ещё \n>Хватит")
				elif pol == 1:
					done = True
				
		botsumm = 0
		botcarts = []
		os.system("cls")
		if summ - 21 <= 0:
			while botsumm < 21:
				if summ - 21 < -2:
					if abs(botsumm - 21) > 6:
						botsumm += cardcreate(2)
					elif abs(botsumm - 21) > 4:
						randbot = random.randint(0, 1)
						if randbot == 1:
							botsumm += cardcreate(2)
					elif abs(botsumm - 21) > 2:
						randbot = random.randint(0, 3)
						if randbot == 1:
							botsumm += cardcreate(2)
					else:
						break
				elif summ - 21 >= -2:
					if abs(botsumm - 21) > 6:
						botsumm += cardcreate(2)
					elif abs(botsumm - 21) > 4:
						randbot = random.randint(0, 2)
						if randbot == 1 or randbot == 2:
							botsumm += cardcreate(2)
					elif abs(botsumm - 21) > 2:
						randbot = random.randint(0, 2)
						if randbot == 1:
							botsumm += cardcreate(2)
					else:
						break
		elif summ - 21 > 0:
			botsumm += cardcreate(2)


		print("Ваши карты:", end=" ")
		for i in range(len(carts)):
			print(carts[i], end=" ")
		print(summ)
		print("Карты бота:", end=" ")
		for i in range(len(botcarts)):
			print(botcarts[i], end=" ")
		print(botsumm)

		if summ - 21 <= 0:
			if botsumm - 21 <= 0:
				if abs(botsumm - 21) < abs(summ-21):
					if overflow > 9:
						print("-=≡", Fore.RED + " Перебор " + Style.RESET_ALL, "≡=-")
					print("-=≡", Fore.RED + " Вы проиграли " + Style.RESET_ALL, "≡=-")
				elif abs(botsumm - 21) > abs(summ-21):
					print("-=≡", Fore.GREEN + "Вы выиграли! " + Style.RESET_ALL, "≡=-")
				else:
					print("-=≡", Fore.BLUE + " Ничья " + Style.RESET_ALL, "≡=-")
			else:
				print("-=≡", Fore.GREEN + "Вы выиграли! " + Style.RESET_ALL, "≡=-")
		else:
			if botsumm - 21 <= 0:
				if overflow > 9:
					print("-=≡", Fore.RED + " Перебор " + Style.RESET_ALL, "≡=-")
				print("-=≡", Fore.RED + " Вы проиграли " + Style.RESET_ALL, "≡=-")
			else:
				print("-=≡", Fore.BLUE + " Ничья " + Style.RESET_ALL, "≡=-")
		
	elif choice == 'n' or choice == 'N' or choice == 'т' or choice == 'Т':
		print('*', 20 * '-', '*', end='\n'
		'|   Спасибо за игру!   |\n')
		print('*', 20 * '-', '*', end='\n')
		flag = False
		break
	else:
		print('!Нужно ввести Y или N в качестве ответа!')
	counter += 1
