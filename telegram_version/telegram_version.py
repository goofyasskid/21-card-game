import telebot
from telebot import types
import config

import random
import array

import colorama
from colorama import init
from colorama import Fore, Back, Style
colorama.init()

bot = telebot.TeleBot(config.token)

deck = [['В', "Д", 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10'], ['В', 'Д', 'К', 'Т', '6', '7', '8', '9', '10']]
decksuit = ['♠', '♥', '♣', '♦']
summ = 0
botsumm = 0
carts = []
botcarts = []
takingcards = True
playing = False
counter = False

@bot.message_handler(commands=['website'])
def open_website(message):
	markup = types.InlineKeyboardMarkup()
	markup.add(types.InlineKeyboardButton("Перейти на сайт", url="https://gitlab.com/sergey_shorin/21-card-game"))
	bot.send_message(message.chat.id,
			"Ссылка на проект на гитлабе",
			parse_mode='html', reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
	markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton('Играть')
	btn2 = types.KeyboardButton('Правила')
	markup.add(btn1, btn2)
	send_mess = f"<b>Привет {message.from_user.first_name}</b>!"
	bot.send_message(message.chat.id, send_mess, parse_mode='html', reply_markup=markup)
	bot.send_message(message.chat.id, 'Я бот для игры в 21,' + '\n' +  'Советую сачала ознакомиться с правилами', parse_mode='html', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def mess(message):
	global playing, deck, summ, botsumm, botcarts, takingcards, counter
	if playing == False:
		get_message_bot = message.text.strip().lower()
		if get_message_bot == "играть" or get_message_bot == "понятно , давай играть":
			playing = True
			markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
			btn1 = types.KeyboardButton('Взять ещё')
			btn2 = types.KeyboardButton('Хватит')
			btn3 = types.KeyboardButton('Напомни правила')
			markup.add(btn1, btn2, btn3)
			bot.send_message(message.chat.id, "Считай сумму карт самостоятельно, если забыл правила, смело жми на кнопку", parse_mode='html', reply_markup=markup)
		elif get_message_bot == "правила":
			markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
			btn1 = types.KeyboardButton('Понятно , давай играть')
			markup.add(btn1)		
			bot.send_message(message.chat.id, 'Правила карточной игры 21:\n\n'
                                              'Цель игры - набрать 21 очко (или число очков,\n  наиболее приближенное к 21) , но не более 21.\n\n'
                                              'Очки начисляются следующим образом:\n'
			                                  'Валет — 2 очка   Дама — 3 очка\nКороль — 4 очка   Туз — 11 очков\n'
                                              'Остальные карты при подсчете очков оцениваются по номиналу.\n\n'
                                              'Возможны следущие исходы:\n'
                                              '     Победа:\n1)Ваши очки меньше или равны 21 и очки бота менее приближены к 21\n2)Ваши очки меньше или равны 21 и очки бота больше 21\n'
                                              '     Проигрыш:\n1)Очки бота меньше или равны 21 и ваши очки менее приближены к 21\n2)Очки бота меньше или равны 21 и ваши очки больше 21\n'
                                              '     Ничья:\n1)Ваши очки равны очкам бота\n2)У обоих сумма очков больше 21', parse_mode='html', reply_markup=markup)                                             
		else:
			markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
			btn1 = types.KeyboardButton('Играть')
			btn2 = types.KeyboardButton('Правила')
			markup.add(btn1, btn2)
			bot.send_message(message.chat.id, "Я не понимаю ;(\nЛучше нажми на одну из кнопок ниже", parse_mode='html', reply_markup=markup)
	if playing == True:


		def cardcreate(x):
			global deck, carts, botcarts
			suit = random.randint(0, 3)
			num = random.randint(0, 8)
			while deck[suit][num] == '':
				num = random.randint(0, 8)
			if x == 2: 
				if decksuit[suit] == '♥' or decksuit[suit] == '♦':
					botcarts.append(decksuit[suit] + deck[suit][num])
				else:
					botcarts.append(decksuit[suit] + deck[suit][num])
			if x == 1:
				if decksuit[suit] == '♥' or decksuit[suit] == '♦':
					carts.append(decksuit[suit] + deck[suit][num])
				else:
					carts.append(decksuit[suit] + deck[suit][num])

			deck[suit][num] = ''
			if num != 3:
				return num + 2
			elif num == 3:
				return 11
		

		get_message_bot = message.text.strip().lower()
		if takingcards == True:
			if get_message_bot == "взять ещё":
				summ += cardcreate(1)
				bot.send_message(message.chat.id,'Твои карты: ' + "  ".join(carts), parse_mode='html')
			if get_message_bot == "хватит":
				takingcards = False
			if get_message_bot == "напомни правила":
				bot.send_message(message.chat.id, 'Правила карточной игры 21:\n\n'
                                              'Цель игры - набрать 21 очко (или число очков,\n  наиболее приближенное к 21) , но не более 21.\n\n'
                                              'Очки начисляются следующим образом:\n'
			                                  'Валет — 2 очка   Дама — 3 очка\nКороль — 4 очка   Туз — 11 очков\n'
                                              'Остальные карты при подсчете очков оцениваются по номиналу.\n\n'
                                              'Возможны следущие исходы:\n'
                                              '     Победа:\n1)Твои очки меньше или равны 21 и очки бота менее приближены к 21\n2)Твои очки меньше или равны 21 и очки бота больше 21\n'
                                              '     Проигрыш:\n1)Очки бота меньше или равны 21 и твои очки менее приближены к 21\n2)Очки бота меньше или равны 21 и твои очки больше 21\n'
                                              '     Ничья:\n1)Твои очки равны очкам бота\n2)У обоих сумма очков больше 21', parse_mode='html')
		
		if takingcards == False:
			if summ - 21 <= 0:
				while botsumm < 21:
					if summ - 21 < -2:
						if abs(botsumm - 21) > 6:
							botsumm += cardcreate(2)
						elif abs(botsumm - 21) > 4:
							randbot = random.randint(0, 1)
							if randbot == 1:
								botsumm += cardcreate(2)
						elif abs(botsumm - 21) > 2:
							randbot = random.randint(0, 3)
							if randbot == 1:
								botsumm += cardcreate(2)
						else:
							break
					elif summ - 21 >= -2:
						if abs(botsumm - 21) > 6:
							botsumm += cardcreate(2)
						elif abs(botsumm - 21) > 4:
							randbot = random.randint(0, 2)
							if randbot == 1 or randbot == 2:
								botsumm += cardcreate(2)
						elif abs(botsumm - 21) > 2:
							randbot = random.randint(0, 2)
							if randbot == 1:
								botsumm += cardcreate(2)
						else:
							break
			elif summ - 21 > 0:
				botsumm += cardcreate(2)
			
			if counter == False:
				if summ - 21 <= 0:
					if botsumm - 21 <= 0:
						if abs(botsumm - 21) < abs(summ-21):
							bot.send_message(message.chat.id, 'Сегодня ты проиграл, но не расстраивайся, завтра будет новый день...', parse_mode='html')
						elif abs(botsumm - 21) > abs(summ-21):
							bot.send_message(message.chat.id, 'Поздравляю, ты обыграл кучку еденичек и ноликов!', parse_mode='html')
						else:
							bot.send_message(message.chat.id, 'Ничья', parse_mode='html')
					else:
						bot.send_message(message.chat.id, 'Поздравляю, ты обыграл кучку еденичек и ноликов!', parse_mode='html')
				else:
					if botsumm - 21 <= 0:
						bot.send_message(message.chat.id, 'Сегодня ты проиграл, но не расстраивайся, завтра будет новый день...', parse_mode='html')
					else:
						bot.send_message(message.chat.id, 'Ничья', parse_mode='html')
				markup = types.ReplyKeyboardRemove(selective=False)
				bot.send_message(message.chat.id, 'Твои карты:  ' + "  ".join(carts) + '\n' + 'Карты бота: ' + "  ".join(botcarts) + '\n' 
				                                  'Твои итоговые очки: ' + str(summ) + '\n' + 'Итоговые очки бота: ' + str(botsumm) , reply_markup=markup)
				counter = True
			else:
				pass

bot.polling(none_stop=True)